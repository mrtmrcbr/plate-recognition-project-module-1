import cv2
import sys
import datetime
import time
import requests
import numpy as np
from imutils import grab_contours
from pytesseract import image_to_string

# TODO: add logging mechanism

# TODO: improve this function for checking right folder is created.
def is_upload_folder_created(folder_name="uploads/"):
    return True

def get_timestamp():
    return datetime.datetime.now().strftime('%d_%m_%Y_%H_%M_%S')

def convert_imagefile_to_string(image):
    return image_to_string(image, lang="eng")

# TODO: update this method for java backend
def send_check_plate_request(plate_text):
    response = requests.get(f"http://localhost:5000/{plate_text}")
    print(response.text)
    if response.status_code == requests.codes.ok:
        return True
    else:
        return False

def process_image(image):
    screen = None

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    filtered = cv2.bilateralFilter(gray, 6, 250, 250)
    edged = cv2.Canny(filtered, 30, 200)
    contours = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = sorted(grab_contours(contours), key=cv2.contourArea, reverse=True)[:10]

    for c in cnts:
        epsilon = 0.06 * cv2.arcLength(c, True)  #NOTE: 0.018'di çalışmadı, 0.06 iyi.
        approx = cv2.approxPolyDP(c, epsilon, True)
        if len(approx) == 4:
            screen = approx
            break

    mask = np.zeros(gray.shape, np.uint8)
    cv2.drawContours(mask, [screen], 0, (255, 255, 255), -1)
    cv2.bitwise_and(image, image, mask=mask)

    x, y = np.where(mask == 255)
    topx, topy = np.min(x), np.min(y)
    bottomx, bottomy = np.max(x), np.max(y)

    return gray[topx:bottomx + 1, topy:bottomy + 1]

def save_image(image,file_path=None):
    if file_path is not None:
        cv2.imwrite(file_path,image)
    else:
        raise Exception("While exporting, Image file name can not be empty")

# TODO: try to divide this function smaller units
def start_video_process(cascade_src = 'src/cars.xml',upload_folder_name="uploads"):

    video_capture_device = cv2.VideoCapture(0)
    car_cascade = cv2.CascadeClassifier(cascade_src)
    image_precess_toggle = True
    working_period = datetime.timedelta(minutes=5)
    process_activated_time = datetime.datetime.now()

    while video_capture_device.isOpened():

        is_readable, video_frame = video_capture_device.read()
        key = cv2.waitKey(1)
        
        if not is_readable:
            cv2.destroyAllWindows()
            raise Exception("Camera/Video source is not valid")
            sys.exit(1)

        grayed_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2GRAY)
        detected_cars = car_cascade.detectMultiScale(grayed_frame, 1.1, 1)

        if len(detected_cars) > 0 and image_precess_toggle:
            image_filename = get_timestamp()
            car_frame_name = f"{upload_folder_name}/{image_filename}.png"
            processed_car_frame_name = f"{upload_folder_name}/{image_filename}_processed.png"

            save_image(video_frame,car_frame_name)

            processed_car_frame = process_image(video_frame)
            if len(processed_car_frame) > 0:
                plate_text = convert_imagefile_to_string(processed_car_frame)

                save_image(processed_car_frame,processed_car_frame_name)

                print("--> ", plate_text)

                if send_check_plate_request(plate_text):
                    process_activated_time = datetime.datetime.now()
                    image_precess_toggle = False
                    print(image_precess_toggle)
                else:
                    # TODO: log unsuccess request
                    pass
            
            for (x,y,w,h) in detected_cars:
                cv2.rectangle(video_frame,(x,y),(x+w,y+h),(0,0,255),2)      
            


        if(datetime.datetime.now() - process_activated_time > working_period) and not image_precess_toggle:
            image_precess_toggle = True
            print(image_precess_toggle)

        if key == ord('q'):
            break

        cv2.imshow('video', video_frame)

    video_capture_device.release()
    cv2.destroyAllWindows()


def main():
    if is_upload_folder_created():
        start_video_process()
    else:
        sys.exit(1)
    
if __name__ == "__main__":
    main()